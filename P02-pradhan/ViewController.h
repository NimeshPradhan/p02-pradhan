//
//  ViewController.h
//  P02-pradhan
//
//  Created by Nimesh on 1/29/17.
//  Copyright © 2017 Nimesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UILabel) NSMutableArray *tilesarray;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;
@property (strong, nonatomic) IBOutlet UILabel *score;
@property (strong, nonatomic) IBOutlet UILabel *highscore;
@property int *highscorevalue;
- (IBAction)speedChange:(id)sender;
@property (strong, nonatomic) IBOutlet UIProgressView *progressBar;
@property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
@property (strong, nonatomic) IBOutlet UIView *complete;
@property (strong, nonatomic) IBOutlet UILabel *congratulationsLabel;
@property int *scorevalue;
@end

