//
//  ViewController.m
//  P02-pradhan
//
//  Created by Nimesh on 1/29/17.
//  Copyright © 2017 Nimesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize tilesarray;
@synthesize resetButton;
@synthesize highscore;
@synthesize score;
@synthesize highscorevalue;
@synthesize gameOverLabel;
@synthesize progressBar;
@synthesize complete;
@synthesize congratulationsLabel;
int currentScore;
BOOL noMove;


- (void)viewDidLoad {
    [super viewDidLoad];
    //add background image
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"images.jpeg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    //set initial values
    [progressBar setProgress:0.0];
    score.text = @"0000";
    [self resetTileNumbers];
    
    
    //guesture
    //left swipe
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    //right swipe
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    //swipe up
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc]  initWithTarget:self action:@selector(didSwipe:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    //swipe down
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
}

- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    //left swipe
    if(swipe.direction == UISwipeGestureRecognizerDirectionLeft){
        int i, j, k, m;
        int merged[4];
        Boolean generateTile = false;
        for(i = 0; i < 16; i=i+4) {
            k=0;
            m = 0;
            for (j = 0; j < 4; j++)
                merged[j] = 0;
            while (k < 4){
                m=0;
                for(j = i; j < i + 3; j++){
                    int currentValue = [[[tilesarray objectAtIndex:j] text] intValue];
                    int nextValue = [[[tilesarray objectAtIndex:j+1] text] intValue];
                    
                    if(currentValue == 0){
                        [[tilesarray objectAtIndex: j] setText: [[tilesarray objectAtIndex:j+1] text]];
                        [[tilesarray objectAtIndex: j + 1] setText: @""];
                        if (nextValue!=0)
                            generateTile = true;
                    }
                    else if(currentValue != nextValue && i % 4 != 0){
                        [[tilesarray objectAtIndex:j] setText : [[tilesarray objectAtIndex:j+1] text]];
                        [[tilesarray objectAtIndex:j + 1] setText : @""];
                        generateTile = true;
                    }
                    else  if(nextValue == currentValue && merged[m]==0 && merged[m+1]==0){
                        [[tilesarray objectAtIndex: j] setText: [NSString stringWithFormat:@"%d",currentValue+nextValue]];
                        merged[m] = 1;
                        [[tilesarray objectAtIndex: j + 1] setText: @""];
                        
                        currentScore = currentValue+nextValue;
                        [self updateScore];
                        generateTile = true;
                    }
                    m++;
          //          NSLog(@"done");
                }
             //   m=0;
                k++;
            }
        }
        if(generateTile){
            [self generateRandomTile];
            [self checkProgress];
            generateTile = false;
            [self setTileBackground];
       }
    }
    //swipe right
    if(swipe.direction == UISwipeGestureRecognizerDirectionRight){
        int i, j, k, m;
        int merged[4];
        Boolean generateTile = false;
        for(i = 15; i > 0; i=i-4) {
            k=0;
            m = 0;
            for (j = 0; j < 4; j++)
                merged[j] = 0;
            while (k < 4){
                m=0;
                for(j = i; j > i - 3; j--){
                    int currentValue = [[[tilesarray objectAtIndex:j] text] intValue];
                    int nextValue = [[[tilesarray objectAtIndex:j-1] text] intValue];
                    
                    if(currentValue == 0){
                        [[tilesarray objectAtIndex: j] setText: [[tilesarray objectAtIndex:j-1] text]];
                        [[tilesarray objectAtIndex: j - 1] setText: @""];
                        if (nextValue!=0) {
                            generateTile = true;
                            noMove = false;
                        }
                    }
                    else if(nextValue == currentValue && merged[m]==0 && merged[m+1]==0) {
                        [[tilesarray objectAtIndex: j] setText: [NSString stringWithFormat:@"%d",currentValue+nextValue]];
                        merged[m] = 1;
                        [[tilesarray objectAtIndex: j - 1] setText: @""];
                        currentScore = currentValue+nextValue;
                        [self updateScore];
                        generateTile = true;
                        noMove = false;
                    }
                    else if(currentValue != nextValue && i!=3 && i!=7 && i!=11 && i!=15){
                        [[tilesarray objectAtIndex:j] setText : [[tilesarray objectAtIndex:j-1] text]];
                        [[tilesarray objectAtIndex:j - 1] setText : @""];
                        generateTile = true;
                        noMove = false;
                    }
                    else
                        noMove = true;
                    m++;
     //               NSLog(@"done");
                }
                //   m=0;
                k++;
            }
        }
        if(generateTile){
            [self generateRandomTile];
            [self checkProgress];
            generateTile = false;
            [self setTileBackground];
        }
    }
    //swipe up
    if(swipe.direction == UISwipeGestureRecognizerDirectionUp){
        int i, j, k, m;
        int merged[4];
        Boolean generateTile = false;
        for(i = 0; i < 4 ; i++) {
            k=0;
            m = 0;
            for (j = 0; j < 3; j++)
                merged[j] = 0;
            while (k < 4){
                m=0;
                for(j = i; j < i + 9; j=j+4){
             //       NSLog(@"j=%d, i=%d", j,i);
                    int currentValue = [[[tilesarray objectAtIndex:j] text] intValue];
                    int nextValue = [[[tilesarray objectAtIndex:j+4] text] intValue];
                    
                    if(currentValue == 0){
                        [[tilesarray objectAtIndex: j] setText: [[tilesarray objectAtIndex:j+4] text]];
                        [[tilesarray objectAtIndex: j + 4] setText: @""];
                        if (nextValue!=0)
                            generateTile = true;
                    }
                    else if(nextValue == currentValue && merged[m]==0 && merged[m+1]==0) {
                        [[tilesarray objectAtIndex: j] setText: [NSString stringWithFormat:@"%d",currentValue+nextValue]];
                        merged[m] = 1;
                        [[tilesarray objectAtIndex: j + 4] setText: @""];
                        currentScore = currentValue+nextValue;
                        [self updateScore];
                        generateTile = true;
                    }
                    else if(currentValue != nextValue && i!=0 && i!=1 && i!=2 && i!=3){
                        [[tilesarray objectAtIndex:j] setText : [[tilesarray objectAtIndex:j + 4] text]];
                        [[tilesarray objectAtIndex:j + 4] setText : @""];
                        generateTile = true;
                    }
                    m++;
             //       NSLog(@"done");
                }
                //   m=0;
                k++;
            }
        }
        if(generateTile){
            [self generateRandomTile];
            [self progressBar];
            generateTile = false;
            [self setTileBackground];
        }
    }
    //swipe down
    if(swipe.direction == UISwipeGestureRecognizerDirectionDown){
   //     NSLog(@"swiped down");
        int i, j, k, m;
        int merged[4];
        Boolean generateTile = false;
        for(i = 15; i > 11 ; i--) {
            k=0;
            m = 0;
            for (j = 0; j < 4; j++)
                merged[j] = 0;
            while (k < 4){
                m=0;
                for(j = i; j > i - 9; j=j-4){
         //           NSLog(@"j=%d, i=%d", j,i);
                    int currentValue = [[[tilesarray objectAtIndex:j] text] intValue];
                    int nextValue = [[[tilesarray objectAtIndex:j-4] text] intValue];
                    
                    if(currentValue == 0){
                        [[tilesarray objectAtIndex: j] setText: [[tilesarray objectAtIndex:j-4] text]];
                        [[tilesarray objectAtIndex: j - 4] setText: @""];
                        if (nextValue!=0)
                            generateTile = true;
                    }
                    else if(nextValue == currentValue && merged[m]==0 && merged[m+1]==0) {
                        [[tilesarray objectAtIndex: j] setText: [NSString stringWithFormat:@"%d",currentValue+nextValue]];
                        merged[m] = 1;
                        [[tilesarray objectAtIndex: j - 4] setText: @""];
                        currentScore = currentValue+nextValue;
                        [self updateScore];
                        generateTile = true;
                    }
                    else if(currentValue != nextValue && i!=12 && i!=13 && i!=14 && i!=15){
                        [[tilesarray objectAtIndex:j] setText : [[tilesarray objectAtIndex:j - 4] text]];
                        [[tilesarray objectAtIndex:j - 4] setText : @""];
                        generateTile = true;
                    }
                    m++;
    //                NSLog(@"done");
                }
                //   m=0;
                k++;
            }
        }
        if(generateTile){
            [self generateRandomTile];
            [self progressBar];
            generateTile = false;
            [self setTileBackground];
        }
    }
}
- (void)generateRandomTile {
    //generate random number between 2 and 4 at randon tile
    int tileNum, num, check;
    check = arc4random_uniform(16);
    if (check >= 12)
        num=2;
    else
        num=4;
    tileNum = arc4random_uniform(16);
    if([[[tilesarray objectAtIndex:tileNum] text] isEqual:@""]){
        [[tilesarray objectAtIndex:tileNum] setText: [NSString stringWithFormat:@"%d",num]];
        [self setTileBackground];
        [self checkGameOver];
    }
    else{
            [self generateRandomTile];
    }
}

- (void)resetTileNumbers {
    int i;
    for(i = 0; i < 16; i++){
        [[tilesarray objectAtIndex:i] setText:@""];
    }    //Generating 1st random Number
    NSInteger n1 = arc4random_uniform(16);
    if([[[tilesarray objectAtIndex:n1] text] isEqual:@""])
    {
        [[tilesarray objectAtIndex:n1] setText: @"2"];
              [self setTileBackground];
        
    }
    //generating second random number
    NSInteger n2 = arc4random_uniform(16);
    if(n2==n1)//checking if same random number is generated.
    {
        n2 = arc4random_uniform(16);
    }
    if([[[tilesarray objectAtIndex:n2] text] isEqual:@""])
    {
        [[tilesarray objectAtIndex:n2] setText: @"2"];
            [self setTileBackground];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetButton:(id)sender {
    score.text = @"0000";
    [gameOverLabel setHidden:true];
    [progressBar setProgress:0.0];
    [highscore setBackgroundColor:[UIColor clearColor]];
   [congratulationsLabel setHidden:true];
    [self resetTileNumbers];
}

- (void)updateScore {
    //update score label
    score.text = [NSString stringWithFormat:@"%d",currentScore+[[score text] intValue]];
    [self updateHighScore];
}

- (void)updateHighScore {
    //update highscore label
    int highscoreInt=[[highscore text] intValue];
    int currentscoreInt = [[score text] intValue];
    if(highscoreInt <= currentscoreInt ){
        highscore.text= [NSString stringWithFormat:@"%d", currentscoreInt];
        //if high score achied then change background color to label
        [highscore setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:240.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
    //    [highscore setBackgroundColor:[UIColor clearColor]];
        
    }
}

- (void)checkGameOver {
   // int count = 0;
    Boolean gameOver = true;;
    //checking for possible moves
    for (int go=0; go<=14; go++)
    {
        if([[[tilesarray objectAtIndex:go] text] isEqual:@""]){
            gameOver = false;
            break;
        }
        else
        if(go == 3 || go ==7 || go == 11){
            if([[[tilesarray objectAtIndex:go] text] isEqual:[[tilesarray objectAtIndex:go+4]text]]){
                gameOver=false;
                break;
            }
        }
        else
            if(go == 12 || go ==13 || go == 14){
                if([[[tilesarray objectAtIndex:go] text] isEqual:[[tilesarray objectAtIndex:go+1]text]]){
                    gameOver=false;
                    break;
                }
            }
        else
            if([[[tilesarray objectAtIndex:go] text] isEqual:[[tilesarray objectAtIndex:go+1]text]] ||
                [[[tilesarray objectAtIndex:go] text] isEqual:[[tilesarray objectAtIndex:go+4]text]]){
                    gameOver = false;
                    break;
        }
    }
    if(gameOver){
        [gameOverLabel setHidden:false];
    }

}

-(void)setTileBackground {
    int i = 0;
    for(i = 0; i < 16; i++){
        int tileNum = [[[tilesarray objectAtIndex:i] text] intValue];
        //change tile bachground color for different numbers
        switch (tileNum) {
            case 0:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f]];
                break;
            case 2:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:230.0f/255.0f green:230.0f/255.0f blue:230.0f/255.0f alpha:1.0f]];
                break;
            case 4:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:255.0f/255.0f blue:215.0f/255.0f alpha:1.0f]];
                break;
            case 8:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:180.0f/255.0f blue:160.0f/255.0f alpha:1.0f]];
                break;
            case 16:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:160.0f/255.0f green:200.0f/255.0f blue:200.0f/255.0f alpha:1.0f]];
                break;
            case 32:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:150.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
                break;
            case 64:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:180.0f/255.0f green:255.0f/255.0f blue:240.0f/255.0f alpha:1.0f]];
                break;
            case 128:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:160.0f/255.0f blue:200.0f/255.0f alpha:1.0f]];
                break;
            case 256:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:161.0f/255.0f blue:180.0f/255.0f alpha:1.0f]];
                break;
            case 512:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:180.0f/255.0f green:200.0f/255.0f blue:174.0f/255.0f alpha:1.0f]];
                break;
            case 1024:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:150.0f/255.0f green:161.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
                break;
            case 2048:
                [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
                break;
            default:
                 [[tilesarray objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:120.0f/255.0f green:161.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
                break;
        }
    }
}

- (void)checkProgress {
    int max;
    int temp;
    max = [[[tilesarray objectAtIndex:0]text]intValue];
    
    for(int i = 1; i< 16; i++){
        temp= [[[tilesarray objectAtIndex:i] text] intValue];
        if(temp > max)
            max=temp;
    }
    //show progress in progress bar, 100% when tile reaches 2048
    switch (max) {
        case 2:
            [progressBar setProgress:0.9];
            break;
        case 4:
            [progressBar setProgress:0.18];
            break;
        case 8:
            [progressBar setProgress:0.27];
            break;
        case 16:
            [progressBar setProgress:0.36];
            break;
        case 32:
            [progressBar setProgress:0.45];
            break;
        case 64:
            [progressBar setProgress:0.54];
            break;
        case 128:
            [progressBar setProgress:0.63];
            break;
        case 256:
            [progressBar setProgress:0.72];
            break;
        case 512:
            [progressBar setProgress:0.81];
            break;
        case 1048:
            [progressBar setProgress:0.90];
            break;
        case 2048:
            [congratulationsLabel setHidden:false];
            [progressBar setProgress:1.0];
            break;        default:
            [progressBar setProgress:1.0];
            break;
    }
}
    
@end
