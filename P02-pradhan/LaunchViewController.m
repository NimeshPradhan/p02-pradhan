//
//  UIViewController+LaunchViewController.m
//  P02-pradhan
//
//  Created by Nimesh on 2/12/17.
//  Copyright © 2017 Nimesh. All rights reserved.
//

#import "LaunchViewController.h"

@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Adding background image to the main screen
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"images.jpeg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
